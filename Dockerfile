#
# Consider this a temporary starting point, until we get a better, custom-built
# base container for the JGI. As it stands, this is over 170 MB for 'hello world'!
FROM debian:jessie
#
# You can try building from an alpine:3.5 base if you want to optimize your image.
# Read https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management for details
# on how to manage packages on alpine linux
#FROM alpine:3.5

#
# Please update this metadata :-)
LABEL Maintainer "Your Name your_email@wherever"

#
# If you need to add more software, try to follow this style:
# - use --no-install-recommends to reduce bloat
# - use command-chaining to keep it all in one cache layer
# - use purge to remove stuff afterwards if you can
#
# Set the DEBIAN_FRONTEND environment variable as shown to avoid problems
# being prompted for input
#
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && \
    apt-get install -y wget ca-certificates --no-install-recommends && \
    apt-get purge -y --auto-remove
RUN apt-get install curl python python-pip build-essential python-dev -y
RUN pip install numpy biopython
RUN curl -O -L http://software.broadinstitute.org/software/gaemr/wp-content/uploads/2012/12/GAEMR-1.0.1.tar.gz && zcat GAEMR-1.0.1.tar.gz | tar -xf - && rm /GAEMR-1.0.1.tar.gz
RUN cd /GAEMR-1.0.1 && python setup.py install
COPY fastaSort.pl /usr/local/bin
COPY jgi_mgaCreateMetaAgp.pl /usr/local/bin
ENV PATH=/usr/local/bin:$PATH
