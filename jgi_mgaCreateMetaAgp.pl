#!/usr/bin/env perl

=head1 NAME

 jgi_mgaCreateMetaAgp.pl

=head1 SYNOPSIS

 jgi_mgaCreateMetaAgp.pl <scaffolds file><output prefix><output dir>

=head1 DESCRIPTION

This script takes a scaffold file a output prefix and info file.

Output is :
    {prefix}.contigs.fasta
    {prefix}.scaffolds.fasta
    {prefix}.agp
    
External dependencies:
    module load gaemr (agp file creation)
    fastaSort.pl sort by length and rename


=head1 VERSION

$Revision:$

$Date:$

=head1 AUTHOR(S)

Brian Foster

=head1 HISTORY

=over

=item *

bfoster 20130913 creation

=back

=cut

use strict; 
use warnings;
use FindBin qw($RealBin);
use Cwd 'abs_path';
use POSIX;

unless(scalar(@ARGV)==3  && -s $ARGV[0] && -d $ARGV[2]){ 
    die "Usage: $0  <scaffolds file><output prefix><output dir>\n";    
}

my $inFasta = $ARGV[0];
my $prefix = $ARGV[1];
my $dir = $ARGV[2];
my $outPrefix = $dir . "/" . $ARGV[1];

#1) clean fasta file (remove lines with blank lines)
my $inFastaCleaned = "${outPrefix}.rawCleaned.fasta";
open (IN, $inFasta) or die "can't open $inFasta\n";
open (OUT, ">$inFastaCleaned") or die "can't open $inFastaCleaned";
while(<IN>){
    if (/^$/){
    }
    else{
	print OUT $_;
    }
}
close(IN);
close(OUT);

#2) sort and rename fasta file using fasta sort

my $cmd = "$RealBin/fastaSort.pl -s 1 $inFastaCleaned $outPrefix";
if (system($cmd)){
    die "error $cmd $!\n";
}

#3) convert fasta to agp using gaemr
$cmd = "make_standard_assembly_files.py -S ${outPrefix}.fasta -r True -o $outPrefix 1> ${outPrefix}.agp.rename";
if (system($cmd)){
    die "error $cmd $!\n";
}

