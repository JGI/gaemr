#!/usr/bin/env perl 

use warnings;
use strict;
use Cwd 'abs_path';
use File::Path;
use Getopt::Long;
use vars qw( $optHelp $optSeedNum);

if( !GetOptions(
                "h"   =>\$optHelp,
                "s=i" =>\$optSeedNum,
                )
    ) {
    printhelp();
}

# check if proper arguments are present, exit w/ help if not                                                                                                                                                                                            #                                                                                                                                                                                                                                                        
if ( defined $optHelp) {
    printhelp();
}

if ($#ARGV != 1) {
    print STDERR "not enough args!\n";

    foreach my $el (@ARGV) {print "arg=$el\n";}

    printhelp();
}



my $fastaFile  = shift @ARGV;
if ($fastaFile !~ /.fasta$/ && $fastaFile !~ /.fa$/){
    die "$fastaFile does not look like it has a fasta file extension\n";
}
elsif(! -e $fastaFile || ! -s $fastaFile){
    die "$fastaFile does not exist or is zerosize\n";
}

# does qual file exist?
my ($qualFile,$qualTest);
if ($fastaFile =~ /\.fasta$/){
    ($qualTest = $fastaFile)=~ s/.fasta$/.qual/;
    if (-e $qualTest){
	$qualFile = $qualTest;
    }
    elsif(-e "$fastaFile.qual"){
	$qualFile = "$fastaFile.qual"
    }
    else{
	#assume no qual file
    }
}
elsif($fastaFile =~ /\.fa$/){
    ($qualTest = $fastaFile)=~s/\.fa$/.qual/;
    if (-e $qualTest){
	$qualFile = $qualTest;
    }
    elsif(-e "$fastaFile.qual"){
	$qualFile = "$fastaFile.qual"
    }
    else{
	#assume no qual file
    }
}
else{
    #no qualfile
}

my $outprefix = shift @ARGV;
if ($outprefix =~ /\.fasta$/ || $outprefix =~ /\.fa$/){
    die "output should be a prefix\nEx. outfile not outfile.fasta\n";
}

my $outputFasta = $outprefix . ".fasta";
if ($qualFile){
    my $outputQual = $outprefix . ".fasta.qual";
}

&large_sort_fasta($fastaFile,$qualFile,$outprefix,$optSeedNum);

sub large_sort_fasta{
    my $file = $_[0];
    my $qual = $_[1];
    my $outprefix = $_[2];
    my $seed = $_[3];

    my $outfile = $outprefix . ".fasta";
    my $outfilequal = $outprefix . ".qual";
    my $tempdir = $outfile  . "." . time();

    my $tempdirexists = (-d $tempdir && -W $tempdir)? 1: 0;
    if (! $tempdirexists){
	unless(mkdir($tempdir)){
	    die "unable to make dir $tempdir\n";
	}
    }
    my $tempfile = $tempdir . "/tmpfile" . time();
    my $tempfilesort = $tempfile . ".sort";

    my ($seqlength,$qualstring,$qualrec,$iqh);
    #get table of seqs
    open (my $ifh ,"<", $file ) or die "Can't open $file\n";
    if ($qual){
	open ($iqh,"<",$qual) or die "Can't open $qual\n";
    }
    open (my $ofh ,">", $tempfile ) or die "Can't open $tempfile\n";
    { 
	local $/ = "\n>";
	my (@temp,$id,$seq,$length);
	while(<$ifh>){
	    s/\n>$//;
	    $qualstring = "";
	    @temp = split("\n",$_);
	    ($id = shift @temp)=~ s/^>//;
	    $seqlength = length($temp[0]) if (!$seqlength);
	    $seq = join("",@temp);
	    $length = length($seq);
	    if ($qual){
		$qualrec = <$iqh>;
		$qualrec =~ s/\n>$//;
		@temp = split("\n",$qualrec);
		($id = shift @temp)=~ s/^>//;
		$qualstring = join(" ",@temp);
		$qualstring =~ s/\s$//;
	    }
	    print $ofh "$length\t$id\t$seq\t$qualstring\n";
	}
    }                  
    close($ifh);
    close($ofh);

    #unix sort
    my $cmd = "sort -T $tempdir -k1,1nr -k2,2  $tempfile >| $tempfilesort";
    if (system($cmd)){
	die "$cmd died $!\n";
    }

    #convert to fasta (and qual)
    my (@F,$ofq,$lfh,$scaffold);
    my $legend = "$outprefix.legend";
    open ($ifh, "<",$tempfilesort) or die "can't open $tempfilesort\n";
    open ($ofh, ">",$outfile) or die "$outfile\n";
    if ($seed){
	open ($lfh, ">",$legend) or die "$legend\n";
	print $lfh "originalName\tsize\tnewName\n";
    }
    if ($qual){
	open ($ofq, ">", $outfilequal) or die "$outfile\n";
    }
    while(my $line = <$ifh>){
	@F=split/\t/,$line;
	if ($seed){
	    $scaffold = "scaffold_" . $seed;
	    print $lfh "$F[1]\t$F[0]\t$scaffold\n";
	    print $ofh ">$scaffold\n";
	    $seed = $seed + 1;
	}
	else{
	    print $ofh ">$F[1]\n";
	}
	print $ofh seq2fasta($F[-2],$seqlength);

	if ($qual){
	    if ($seed){
		print $ofq ">$scaffold\n";
	    }
	    else{
		print $ofq ">$F[1]\n";
	    }
	    print $ofq qual2fasta($F[-1],int($seqlength/2));
	}
    }
    close($ifh);
    close($ofh);
    if($seed){
	close($lfh);
    }
    if ($qual){
	close($ofq);
    }

    rmtree($tempdir);
}

sub seq2fasta{
    my $seq = $_[0];
    my $seqlength = $_[1];
    $seq =~ s/(.{$seqlength})/$1\n/g;
    ($seq .= "\n") =~ s/\n+$/\n/;
    return $seq;
}

sub qual2fasta{
    my $qual = $_[0];
    my $seqlength = $_[1];
    my @quals = split(/\s/,$qual);
    my $counter = 0;
    my $qualstringwithreturns = "";
    foreach my $qual (@quals){
	$qualstringwithreturns .= " " if ($counter != 0 );
	$qualstringwithreturns .=$qual;
	if ($counter++ == $seqlength){
	    $qualstringwithreturns .= "\n";
	    $counter = 0;
	}
    }
    $qualstringwithreturns =~ s/^\s+//;
    $qualstringwithreturns =~ s/\n$//m;
    $qualstringwithreturns .= "\n";
    return $qualstringwithreturns;
}

#####################################################                                                                                                                                                                                                    
sub printhelp {
    print << "HELP";

    Usage: $0 -s <integer> <fasta_file> <output_name>

           -s  seed number to start numbering scaffolds with (optional)

           This script takes a multi fasta file and reorders/(optionally renames) the
           fasta in order of decreasing size.  The numbering starts at
           -s <integer> and is incremented by 1 for each entry.  Also
           outputs a <output_name>.legend so you can map between the
           original and newly named scaffolds.

	 Note: This script performs a unix sort to disk to accomodate large files.
	       It is not recommended to run many concurrent runs on a single disk.
HELP
exit 1;
}


